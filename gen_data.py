"""  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
template for generating data to fool learners (c) 2016 Tucker Balch  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Atlanta, Georgia 30332  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
All Rights Reserved  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Template code for CS 4646/7646  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Georgia Tech asserts copyright ownership of this template and all derivative  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
works, including solutions to the projects assigned in this course. Students  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
and other users of this template code are advised not to share it with others  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or to make it available on publicly viewable websites including repositories  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
such as github and gitlab.  This copyright statement should not be removed  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or edited.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
We do grant permission to share solutions privately with non-students such  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
as potential employers. However, sharing with other current or future  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
students of CS 7646 is prohibited and subject to being investigated as a  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
GT honor code violation.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
-----do not edit anything above this line---  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
import numpy as np  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			

# this function should return a dataset (X and Y) that will work  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
# better for linear regression than decision trees
def best_4_lin_reg(seed=1489683273):
    """
    best4LinReg() should return data that performs significantly better (see rubric) with LinRegLearner than DTLearner.

    Each data set should include from 2 to 10 columns in X, and one column in Y. The data should contain from 10 (minimum) to 1000 (maximum) rows.
    """

    '''np.random.seed(seed)  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    X = np.zeros((100,2))  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    Y = np.random.random(size = (100,))*200-100  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # Here's is an example of creating a Y from randomly generated  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # X with multiple columns  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # Y = X[:,0] + np.sin(X[:,1]) + X[:,2]**2 + X[:,3]**3'''

    np.random.seed(seed)

    num_rows = np.random.randint(10, 1001)
    num_cols = np.random.randint(2, 11)

    X = np.random.normal(size=(num_rows, num_cols))
    Y = np.zeros(num_rows)
    for c in range(num_cols):
        Y += X[:, c]

    return X, Y  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
def best_4_dt(seed=1489683273):
    """
    best4DT() should return data that performs significantly better with DTLearner than LinRegLearner.

    Each data set should include from 2 to 10 columns in X, and one column in Y. The data should contain from 10 (minimum) to 1000 (maximum) rows.
    """

    '''np.random.seed(seed)  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    X = np.zeros((100,2))  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    Y = np.random.random(size = (100,))*200-100'''

    np.random.seed(seed)

    num_rows = np.random.randint(10, 1001)
    num_cols = np.random.randint(2, 11)

    X = np.random.normal(size=(num_rows, num_cols))
    Y = np.zeros(num_rows)

    for c in range(num_cols):
       Y = X[:, c] ** 4 + X[:, c] ** 2

    return X, Y  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
def author():  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    return 'bsheffield7'
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
if __name__=="__main__":  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    print("they call me Tim.")  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
